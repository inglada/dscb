# This code is awful because it is automatically extracted from the
# slides. It is provided for those who don't know the power of Emacs Org
# Mode for executable documents.

import pandas as pd
DATA_DIR = "./"
FR_MIPY = ("FR_MIPY_GAI-E_IMesu_2013.csv",
           "FR_MIPY_ReflectancesOE_IMesu_2013.csv")
LAI_file = f"{DATA_DIR}/{FR_MIPY[0]}"
SAT_file = f"{DATA_DIR}/{FR_MIPY[1]}"

lai_df = pd.read_csv(LAI_file)
sat_df = pd.read_csv(SAT_file)

print(lai_df.columns)
print(sat_df.columns)

print(lai_df[['ID', 'X', 'Y']])
print(sat_df[['ID', 'X', 'Y']])

dates_lai = [int(d) for d in lai_df.columns if d.startswith('20')]
print(dates_lai)

dates_ndvi = sorted(list(set(sat_df.Date)))
print(dates_ndvi)

all_dates = sorted(list(set(dates_lai + dates_ndvi)))
print(all_dates)

columns = ['ID', 'X', 'Y', 'LC', 'CODE', 'date', 'LAI', 'NDVI']

lai_points = lai_df[['ID', 'X', 'Y']].drop_duplicates()
sat_points = sat_df[['ID', 'X', 'Y']].drop_duplicates()
print(lai_points)
print(sat_points)

df = pd.DataFrame(columns=columns)
print(df.head())

import numpy as np
for idx, point in lai_points.iterrows():
    lc = lai_df[lai_df.ID == point['ID']].iloc[0]['LC']
    code = lai_df[lai_df.ID == point['ID']].iloc[0]['CODE']
    new_row = {
        'ID': point['ID'],
        'X': point['X'],
        'Y': point['Y'],
        'LC': lc,
        'CODE': code
    }

    for date in all_dates:
        lai = np.nan
        ndvi = np.nan
        if date in dates_lai:
            lai = lai_df.loc[lai_df.ID == point['ID']][str(date)].iloc[0]
            if lai < 0:
                lai = np.nan
        if date in dates_ndvi:
            red, nir = sat_df.loc[(sat_df.ID == point['ID'])
                                  & (sat_df.Date == date)][['Red',
                                                            'NIR']].iloc[0]
            if red > -0.5:
                ndvi = (nir - red) / (nir + red + 0.1)
        new_row['date'] = date
        new_row['LAI'] = lai
        new_row['NDVI'] = ndvi
        df = df.append(new_row, ignore_index=True)
df['date'] = pd.to_datetime(df['date'], format='%Y%m%d')
df.set_index('date', inplace=True)

matplot_lib_filename = "lai-vs-ndvi.png"
import matplotlib.pyplot as plt
df.plot.scatter('LAI', 'NDVI')
plt.savefig(matplot_lib_filename)
matplot_lib_filename

df['LAI_int'] = 0.0
df['NDVI_int'] = 0.0

for id in df['ID'].unique():
    df.loc[df.ID == id,
           'LAI_int'] = df[df.ID == id]['LAI'].interpolate(method='akima',
                                                           order=5,
                                                           inplace=False)
    df.loc[df.ID == id,
           'NDVI_int'] = df[df.ID == id]['NDVI'].interpolate(method='akima',
                                                             order=5,
                                                             inplace=False)

df.to_csv("lai_ndvi.csv")

import pandas as pd
df = pd.read_csv("lai_ndvi.csv")

matplot_lib_filename = "LAI_int_vs_NDVI_int_per_crop.svg"
import seaborn as sns
color_labels = df['LC'].unique()

# List of colors in the color palettes
rgb_values = sns.color_palette("bright", len(color_labels))

color_map = dict(zip(color_labels, rgb_values))

_, ax = plt.subplots()
for key, group in df.groupby('LC'):
    group.plot.scatter(ax=ax,
                       x='NDVI_int',
                       y='LAI_int',
                       label=key,
                       color=color_map[key])
plt.savefig(matplot_lib_filename)
plt.close()
matplot_lib_filename

matplot_lib_filename = "temporal_profile_LAI_int_NDVI_int.svg"
df[df.ID == 6][['LAI_int', 'NDVI_int']].plot()

plt.savefig(matplot_lib_filename)
matplot_lib_filename

matplot_lib_filename = "ndvi_profiles_ex.svg"
import matplotlib.pyplot as plt
plt.subplots()
df = pd.read_csv("lai_ndvi.csv", parse_dates=["date"])
df['doy'] = df['date'].dt.dayofyear
plt.plot(df[df.ID == 6]['doy'], df[df.ID == 5]['NDVI_int'], label="Wheat")
plt.plot(df[df.ID == 6]['doy'], df[df.ID == 23]['NDVI_int'], label="Maize")
plt.legend()
plt.xlabel("DoY")
plt.ylabel("NDVI")

plt.savefig(matplot_lib_filename)
matplot_lib_filename

dfna = df[['LC', 'LAI_int', 'NDVI_int']].dropna().sort_values(by='NDVI_int')
ydata = dfna['LAI_int'].to_numpy()
xdata = dfna['NDVI_int'].to_numpy()

from scipy.optimize import curve_fit
import numpy as np


def lai_from_ndvi(ndvi, a, b, c):
    return a * np.exp(b * ndvi) + c


popt, pcov = curve_fit(lai_from_ndvi, xdata, ydata)

print(f"Optimal (a, b, c) = {popt}")
print(f"Parameter stds = {np.sqrt(pcov.diagonal())}")
print(f"Parameter covariances\n{pcov}")

a_mean, b_mean, c_mean = popt
a_var, b_var, c_var = pcov.diagonal()

matplot_lib_filename = "curve_fit.svg"

_, ax = plt.subplots()
for key, group in df.groupby('LC'):
    group.plot.scatter(ax=ax,
                       x='NDVI_int',
                       y='LAI_int',
                       label=key,
                       color=color_map[key])
ax.plot(xdata,
        lai_from_ndvi(xdata, *popt),
        label='fit: %5.3f exp(%5.3f x) +(%5.3f)' % tuple(popt),
        color='red')
ax.legend()
plt.savefig(matplot_lib_filename)
matplot_lib_filename

matplot_lib_filename = "classicalgausspairplot.png"
import scipy.stats as stats
import math

_, ax = plt.subplots()
mu = a_mean
sigma = math.sqrt(a_var)
x = np.linspace(mu - 3 * sigma, mu + 3 * sigma, 100)
ax.plot(x, stats.norm.pdf(x, mu, sigma))

plt.savefig(matplot_lib_filename)
matplot_lib_filename

# import numpy as np
import jax.numpy as jnp

import logging
import matplotlib.pyplot as plt
import numpy as np
import jax.numpy as jnp
from jax import random, vmap
import os
import pandas as pd
import numpyro
import numpyro.distributions as dist
import numpyro.optim as optim
import seaborn as sns
import sys

plt.style.use('default')

logging.basicConfig(format='%(message)s', level=logging.INFO)
# Enable validation checks
numpyro.enable_validation(True)


def lai_from_ndvi_det(ndvi, a, b, c):
    return a * jnp.exp(b * ndvi) + c


def lai_from_ndvi_prob(ndvi, lai=None):
    a = numpyro.sample("a", dist.Normal(a_mean, a_var * 20))
    b = numpyro.sample("b", dist.Normal(b_mean, b_var * 20))
    c = numpyro.sample("c", dist.Normal(c_mean, c_var * 20))
    sigma_lai = numpyro.sample("sigma_lai", dist.Uniform(0., 4.))
    mean = lai_from_ndvi_det(ndvi, a, b, c)
    with numpyro.plate("data", len(ndvi)):
        lai_samples = numpyro.sample("obs",
                                     dist.Normal(mean, sigma_lai),
                                     obs=lai)
    return lai_samples


matplot_lib_filename = "model_1_sample_generation_prior.svg"
_, ax = plt.subplots()
with numpyro.handlers.seed(rng_seed=4):
    for _ in range(5):
        ax.scatter(xdata,
                   lai_from_ndvi_prob(xdata),
                   marker='.',
                   label="Simulations")
ax.scatter(xdata, ydata, label='Observations')
ax.legend()
ax.grid()
plt.savefig(matplot_lib_filename)
matplot_lib_filename

rng_key = random.PRNGKey(1)
rng_key, rng_key_ = random.split(rng_key)

from numpyro.infer import MCMC, NUTS
nuts_kernel = NUTS(lai_from_ndvi_prob)
warmup_steps = 200
num_samples = 10000
mcmc = MCMC(nuts_kernel, warmup_steps, num_samples=num_samples, num_chains=1)
posterior = mcmc.run(rng_key, xdata, ydata)
posterior_samples = mcmc.get_samples()

import arviz as az
from numpyro.infer import Predictive
predictive = Predictive(lai_from_ndvi_prob, posterior_samples)
posterior_predictive = predictive(rng_key, xdata)
predictive_pars = Predictive(lai_from_ndvi_prob,
                             posterior_samples,
                             return_sites=("a", "b", "c", "sigma_lai"))
posterior_predictive_pars = predictive_pars(rng_key, xdata)
pyro_data = az.from_numpyro(mcmc,
                            posterior_predictive=posterior_predictive,
                            coords={"ndvi": np.arange(xdata.shape[0])},
                            dims={"data": ["lai"]})

matplot_lib_filename = "lai_pars_correlation.svg"
az.plot_pair(pyro_data,
             var_names=['a', 'b', 'c'],
             kind='kde',
             textsize=18,
             figsize=(10, 5))
plt.tight_layout()
plt.savefig(matplot_lib_filename)
matplot_lib_filename


def summary(samples):
    site_stats = {}
    for site_name, values in samples.items():
        marginal_site = pd.DataFrame(values)
        describe = marginal_site.describe(
            percentiles=[.05, 0.25, 0.5, 0.75, 0.95]).transpose()
        site_stats[site_name] = describe[[
            "mean", "std", "5%", "25%", "50%", "75%", "95%"
        ]]
    return site_stats


hmc_samples = {k: v for k, v in mcmc.get_samples().items()}
for site, values in summary(hmc_samples).items():
    print("Site: {}".format(site))
    print(f"{values}\n")

matplot_lib_filename = "lai_marginal_posteriors_parameters.svg"
import scipy.stats as stats
import math

sites = ["a", "b", "c", "sigma_lai"]

pcov_pars = {
    "a": {
        'mean': a_mean,
        'std': math.sqrt(a_var)
    },
    "b": {
        'mean': b_mean,
        'std': math.sqrt(b_var)
    },
    "c": {
        'mean': c_mean,
        'std': math.sqrt(a_var)
    }
}

fig, axs = plt.subplots(nrows=2, ncols=2, figsize=(12, 4))
fig.suptitle("Marginal Posterior density - Model coefficients", fontsize=12)
for i, ax in enumerate(axs.reshape(-1)):
    site = sites[i]
    sns.histplot(hmc_samples[site],
                 stat='density',
                 kde=True,
                 ax=ax,
                 label="HMC")
    if site != "sigma_lai":
        mu = pcov_pars[site]['mean']
        sigma = pcov_pars[site]['std']
        x = np.linspace(mu - 3 * sigma, mu + 3 * sigma, 100)
        sns.lineplot(x=x,
                     y=stats.norm.pdf(x, mu, sigma),
                     label="Curve fit",
                     color='red',
                     ax=ax)
    ax.set_title(site)
    ax.legend(loc='best')
handles, labels = ax.get_legend_handles_labels()
fig.tight_layout()
plt.savefig(matplot_lib_filename)
matplot_lib_filename

matplot_lib_filename = "lai_hdi.svg"
color_labels = df['LC'].unique()

# List of colors in the color palettes
rgb_values = sns.color_palette("bright", len(color_labels))

color_map = dict(zip(color_labels, rgb_values))

_, ax = plt.subplots()
az.plot_hdi(xdata, posterior_predictive['obs'], hdi_prob=0.95)
az.plot_hdi(xdata, posterior_predictive['obs'], hdi_prob=0.5)
for key, group in df.groupby('LC'):
    group.plot.scatter(ax=ax,
                       x='NDVI_int',
                       y='LAI_int',
                       label=key,
                       color=color_map[key])
ax.plot(xdata, lai_from_ndvi(xdata, *popt), label='Curve fit', color='red')
ax.grid()
ax.legend()
ax.set_title("Posterior predictive HDI for 50% and 95%")
plt.savefig(matplot_lib_filename)
matplot_lib_filename

import logging
import matplotlib.pyplot as plt
import numpy as np
import jax.numpy as jnp
from jax import random, vmap
import os
import pandas as pd
import numpyro
import numpyro.distributions as dist
import numpyro.optim as optim
import seaborn as sns
import sys
from numpyro.infer import Predictive
import arviz as az

plt.style.use('default')

logging.basicConfig(format='%(message)s', level=logging.INFO)
# Enable validation checks
numpyro.enable_validation(True)


def sigmo(x, x0, x1, epsilon=1e-6):
    return 1 / (1 + jnp.exp((x0 - x) / (x1 + epsilon)))


def double_sigmo(x, A, B, x0, x1, x2, x3):
    return A * (sigmo(x, x0, x1) - sigmo(x, x2, x3)) + B


def pheno_sigmo(x, max_ndvi, min_ndvi, sos, maturity, senescense, eos):
    A = max_ndvi - min_ndvi
    B = min_ndvi
    x0 = (sos + maturity) / 2
    x2 = (senescense + eos) / 2
    x1 = A * (maturity - sos) / (4 * (A - B))
    x3 = A * (eos - senescense) / (4 * (A - B))
    return double_sigmo(x, A, B, x0, x1, x2, x3)


matplot_lib_filename = "sigmosimuex.svg"
x = jnp.linspace(0, 365, 100)
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.plot(x, pheno_sigmo(x, 0.7, 0.1, 100, 150, 250, 280))
ax.grid()
plt.savefig(matplot_lib_filename)
matplot_lib_filename


def pheno_model_complete(x, obs=None):
    min_ndvi = numpyro.sample("min_ndvi", dist.Uniform(0, 0.4))
    max_ndvi = numpyro.sample("max_ndvi", dist.Uniform(min_ndvi + 0.3, 1))
    summer_crop_like = numpyro.sample("summer_crop", dist.Uniform(0, 1))
    offset = summer_crop_like * 90 + 30
    sos = numpyro.sample("sos", dist.Uniform(offset, 90 + offset))
    maturity = numpyro.sample("maturity", dist.Uniform(sos, sos + 90))
    senescense = numpyro.sample("senescense",
                                dist.Uniform(maturity, maturity + 90))
    eos = numpyro.sample("eos", dist.Uniform(senescense, senescense + 90))
    sigma = numpyro.sample("sigma", dist.Uniform(0., 0.3))
    mean = pheno_sigmo(x, max_ndvi, min_ndvi, sos, maturity, senescense, eos)
    with numpyro.plate("data", len(x)):
        pheno_profile = numpyro.sample("obs",
                                       dist.Normal(mean, sigma),
                                       obs=obs)
    return (pheno_profile, summer_crop_like, max_ndvi, min_ndvi, sos, maturity,
            senescense, eos)


def pheno_model(x, obs=None):
    return pheno_model_complete(x, obs)[0]


matplot_lib_filename = "phenosimus.svg"
x = jnp.linspace(0, 365, 365 // 5)
fig, axs = plt.subplots(nrows=4, ncols=3, sharey=True, figsize=(12, 8))
with numpyro.handlers.seed(rng_seed=5):
    for i, ax in enumerate(axs.reshape(-1)):
        p, summer_crop, max_ndvi, min_ndvi, sos, maturity, senescense, eos = pheno_model_complete(
            x)
        legend = f"{min_ndvi:.2f}, {max_ndvi:.2f},\n {int(sos)}, {int(maturity)},\n {int(senescense)}, {int(eos)}"
        color = 'red' if summer_crop > 0.5 else 'green'
        ax.plot(x, p, label=legend, color=color)
        ax.grid()
        ax.legend(loc='best')
plt.savefig(matplot_lib_filename)
matplot_lib_filename

df = pd.read_csv("lai_ndvi.csv", parse_dates=["date"])
df['doy'] = df['date'].dt.dayofyear
df_ndvi = df[['doy', 'ID', 'NDVI', 'LC']].sort_values(by='doy').dropna()

matplot_lib_filename = "ndvi_profiles.svg"
fig, axs = plt.subplots(nrows=5, ncols=5, figsize=(8, 8))
for i, ax in enumerate(axs.reshape(-1)):
    doy = df_ndvi[df.ID == i + 1]['doy'].dropna().values
    ndvi = df_ndvi[df.ID == i + 1]['NDVI'].dropna().values
    crop_type = str(df_ndvi[df.ID == i + 1]['LC'].dropna().values[0])
    ax.plot(doy, ndvi, label=f"{i+1} {crop_type}")
    ax.grid()
    ax.legend(loc='best')
plt.tight_layout()
plt.savefig(matplot_lib_filename)
matplot_lib_filename

id = 23
doy = jnp.array(df_ndvi[df.ID == id]['doy'].dropna().values)
ndvi = jnp.array(df_ndvi[df.ID == id]['NDVI'].dropna().values)

matplot_lib_filename = "selected_profile.svg"
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.plot(doy, ndvi)
ax.grid()
plt.savefig(matplot_lib_filename)
matplot_lib_filename

from numpyro.infer import MCMC, NUTS
# Start from this source of randomness. We will split keys for subsequent operations.
rng_key = random.PRNGKey(1)
rng_key, rng_key_ = random.split(rng_key)
nuts_kernel = NUTS(pheno_model)
num_samples = 10000
warmup_steps = 200
mcmc = MCMC(nuts_kernel, warmup_steps, num_samples, num_chains=1)

posterior = mcmc.run(rng_key_, doy, ndvi)

posterior_samples = mcmc.get_samples()


def summary(samples):
    site_stats = {}
    for site_name, values in samples.items():
        marginal_site = pd.DataFrame(values)
        describe = marginal_site.describe(
            percentiles=[.05, 0.25, 0.5, 0.75, 0.95]).transpose()
        site_stats[site_name] = describe[[
            "mean", "std", "5%", "25%", "50%", "75%", "95%"
        ]]
    return site_stats


hmc_samples = {k: v for k, v in mcmc.get_samples().items()}
for site, values in summary(hmc_samples).items():
    print("Site: {}".format(site))
    print(f"{values}\n")

matplot_lib_filename = "pheno_marginal_posteriors_parameters.svg"
sites = [
    "max_ndvi", "min_ndvi", "sos", "maturity", "senescense", "eos",
    "summer_crop", "sigma"
]

fig, axs = plt.subplots(nrows=4, ncols=2, figsize=(8, 8))
fig.suptitle("Marginal Posterior density - Model coefficients", fontsize=16)
for i, ax in enumerate(axs.reshape(-1)):
    if i < len(sites):
        site = sites[i]
        sns.histplot(hmc_samples[site],
                     stat='density',
                     kde=True,
                     ax=ax,
                     label="HMC")
        ax.set_title(site)
        ax.legend(loc='best')
handles, labels = ax.get_legend_handles_labels()
fig.tight_layout()
plt.savefig(matplot_lib_filename)
matplot_lib_filename

predictive = Predictive(pheno_model, posterior_samples)
posterior_predictive = predictive(rng_key, doy)
predictive_pars = Predictive(pheno_model,
                             posterior_samples,
                             return_sites=("sos", "eos", "maturity",
                                           "senescense", "_RETURN"))
posterior_predictive_pars = predictive_pars(rng_key, doy)

pyro_data = az.from_numpyro(mcmc,
                            posterior_predictive=posterior_predictive,
                            coords={"doy": np.arange(doy.shape[0])},
                            dims={"data": ["ndvi"]})
# pyro_data.to_netcdf("posterior_ndvi_profile.netcdf")

matplot_lib_filename = "pheno_hdi.svg"
import arviz as az
_, ax = plt.subplots()
az.plot_hdi(doy, posterior_predictive['obs'], hdi_prob=0.95)
az.plot_hdi(doy, posterior_predictive['obs'], hdi_prob=0.5)
ax.scatter(doy, ndvi, label="Observations", marker=".")
ax.plot(doy, ndvi)
ax.grid()
ax.legend()
ax.set_title("Posterior predictive HDI for 50% and 95%")
plt.savefig(matplot_lib_filename)
matplot_lib_filename

matplot_lib_filename = "summer_pars_correlation.svg"
az.plot_pair(pyro_data,
             var_names=[
                 'sos', 'maturity', 'senescense', 'eos', 'max_ndvi',
                 'min_ndvi', 'summer_crop'
             ],
             kind='kde',
             textsize=10,
             figsize=(10, 10))
plt.tight_layout()
plt.savefig(matplot_lib_filename)
matplot_lib_filename

id = 5
doy_w = jnp.array(df_ndvi[df.ID == id]['doy'].dropna().values)
ndvi_w = jnp.array(df_ndvi[df.ID == id]['NDVI'].dropna().values)

matplot_lib_filename = "selected_profile_w.svg"
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.plot(doy_w, ndvi_w)
ax.grid()
plt.savefig(matplot_lib_filename)
matplot_lib_filename

nuts_kernel_w = NUTS(pheno_model)
num_samples = 10000
warmup_steps = 200
mcmc_w = MCMC(nuts_kernel_w, warmup_steps, num_samples, num_chains=1)

posterior_w = mcmc_w.run(rng_key_, doy_w, ndvi_w)

posterior_samples_w = mcmc_w.get_samples()

predictive_w = Predictive(pheno_model, posterior_samples_w)
posterior_predictive_w = predictive_w(rng_key, doy_w)
predictive_pars_w = Predictive(pheno_model,
                               posterior_samples_w,
                               return_sites=("sos", "eos", "maturity",
                                             "senescense", "_RETURN"))
posterior_predictive_pars_w = predictive_pars_w(rng_key, doy_w)


pyro_data_w = az.from_numpyro(mcmc_w, posterior_predictive=posterior_predictive_w, coords={"doy": np.arange(doy_w.shape[0])}, dims={"data": ["ndvi"]})
pyro_data_w.to_netcdf("posterior_ndvi_profile_w.netcdf")

matplot_lib_filename="pheno_hdi_w.svg"
import arviz as az
_, ax = plt.subplots()
az.plot_hdi(doy_w, posterior_predictive_w['obs'], hdi_prob = 0.95)
az.plot_hdi(doy_w, posterior_predictive_w['obs'], hdi_prob = 0.5)
ax.scatter(doy_w, ndvi_w, label="Observations", marker=".")
ax.plot(doy_w, ndvi_w)
ax.grid()
ax.legend()
ax.set_title("Posterior predictive HDI for 50% and 95%")
plt.savefig(matplot_lib_filename)
matplot_lib_filename

hmc_samples_w = {k: v for k, v in mcmc_w.get_samples().items()}
for site, values in summary(hmc_samples_w).items():
    print("Site: {}".format(site))
    print(f"{values}\n")

matplot_lib_filename="pheno_marginal_posteriors_parameters_w.svg"
sites = ["max_ndvi", "min_ndvi", "sos", "maturity", "senescense", "eos", "summer_crop", "sigma"]

fig, axs = plt.subplots(nrows=4, ncols=2, figsize=(8, 8))
fig.suptitle("Marginal Posterior density - Model coefficients", fontsize=16)
for i, ax in enumerate(axs.reshape(-1)):
    if i < len(sites):
        site = sites[i]
        sns.histplot(hmc_samples_w[site], stat='density', kde=True, ax=ax, label="HMC")
        ax.set_title(site)
        ax.legend(loc='best')
handles, labels = ax.get_legend_handles_labels()
fig.tight_layout()
plt.savefig(matplot_lib_filename)
matplot_lib_filename

matplot_lib_filename="pheno_marginal_posteriors_incr.svg"
sites = ["sos", "maturity", "senescense", "summer_crop"]
fig, axs = plt.subplots(nrows=4, ncols=5,sharex='col', figsize=(16, 8))
fig.suptitle("Incremental inference for a maize NDVI time profile", fontsize=16)
data_iters = []
for i in range(4):
    last_obs = int(len(doy) * (i +1)/4)
    tmp_doy = doy[:last_obs]
    tmp_ndvi = ndvi[:last_obs]
    tmp_nuts_kernel = NUTS(pheno_model)
    num_samples=10000
    warmup_steps=200
    tmp_mcmc = MCMC(tmp_nuts_kernel, warmup_steps, num_samples, num_chains = 1)
    tmp_posterior = tmp_mcmc.run(rng_key_, tmp_doy, tmp_ndvi)
    tmp_posterior_samples = tmp_mcmc.get_samples()
    tmp_hmc_samples = {k: v for k, v in tmp_mcmc.get_samples().items()}
    tmp_pred = Predictive(pheno_model, tmp_posterior_samples)
    tmp_posterior_predictive = tmp_pred(rng_key, doy)
    ax = axs[i]
    sns.histplot(tmp_hmc_samples["sos"], stat='density', kde=True, ax=ax[0])
    ax[0].set_title('SOS')
    sns.histplot(tmp_hmc_samples["maturity"], stat='density', kde=True, ax=ax[1])
    ax[1].set_title('Maturity')
    sns.histplot(tmp_hmc_samples["senescense"], stat='density', kde=True, ax=ax[2])
    ax[2].set_title('Senescense')
    sns.histplot(tmp_hmc_samples["summer_crop"], stat='density', kde=True, ax=ax[3])
    ax[3].set_title('SummerLike')
    az.plot_hdi(doy, tmp_posterior_predictive['obs'], hdi_prob = 0.95, ax=ax[4])
    az.plot_hdi(doy, tmp_posterior_predictive['obs'], hdi_prob = 0.5, ax=ax[4])
    ax[4].plot(tmp_doy, tmp_ndvi, label="Observations")
    ax[4].scatter(tmp_doy, tmp_ndvi, marker=".")
    ax[4].grid()
    ax[4].set_title("HDI 50% and 95%")
    data_iters.append({'doy': tmp_doy, 'ndvi': tmp_ndvi, 'samples': tmp_hmc_samples, 'predictive': tmp_posterior_predictive})
fig.tight_layout()
plt.savefig(matplot_lib_filename)
matplot_lib_filename

for i in range(4):
    fig, ax = plt.subplots(nrows=1, ncols=5, figsize=(16, 5))
    tmp_hmc_samples = data_iters[i]['samples']
    tmp_posterior_predictive = data_iters[i]['predictive']
    tmp_doy = data_iters[i]['doy']
    tmp_ndvi = data_iters[i]['ndvi']
    fig.suptitle("Incremental inference for a maize NDVI time profile", fontsize=16)
    sns.histplot(tmp_hmc_samples["sos"], stat='density', kde=True, ax=ax[0])
    ax[0].set_xlim(0, 366)
    ax[0].set_title('SOS')
    sns.histplot(tmp_hmc_samples["maturity"], stat='density', kde=True, ax=ax[1])
    ax[1].set_xlim(0, 366)
    ax[1].set_title('Maturity')
    sns.histplot(tmp_hmc_samples["senescense"], stat='density', kde=True, ax=ax[2])
    ax[2].set_xlim(0, 366)
    ax[2].set_title('Senescense')
    sns.histplot(tmp_hmc_samples["summer_crop"], stat='density', kde=True, ax=ax[3])
    ax[3].set_xlim(0, 1)
    ax[3].set_title('Summer Like')
    az.plot_hdi(doy, tmp_posterior_predictive['obs'], hdi_prob = 0.95, ax=ax[4])
    az.plot_hdi(doy, tmp_posterior_predictive['obs'], hdi_prob = 0.5, ax=ax[4])
    ax[4].plot(tmp_doy, tmp_ndvi, label="Observations")
    ax[4].scatter(tmp_doy, tmp_ndvi, marker=".")
    ax[4].grid()
    ax[4].set_title("HDI 50% and 95%")
    fig.tight_layout()
    plt.savefig(f"pheno_marginal_posteriors_incr_{i}.svg")
