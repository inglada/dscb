import numpy as np
from sklearn.linear_model import LinearRegression as LR
import pandas as pd
from scipy.linalg import inv, solve
import matplotlib.pyplot as plt

# Data
np.random.seed(0)
x = 3*np.random.rand(20)[:, np.newaxis]
y = 2*x -1 + 0.2*np.random.randn(x.size, 1)

# Model
model = LR()
model.fit(x, y)

pd.DataFrame.from_dict({"x": x.squeeze().tolist(),
                        "y": y.squeeze().tolist()}).to_csv("./../figures/linear_regression_train.csv")

print(model.coef_)
print(model.intercept_)
print(np.var(y-model.predict(x)))

# Construction of Phi
alpha=0
Phi = np.ones((x.shape[0], 2))
Phi[:, 1] = x.squeeze()

mu_n = solve(np.dot(Phi.T, Phi) + (alpha)*np.eye(2), np.dot(Phi.T, y))
sigma_n = inv(np.dot(Phi.T, Phi) + (alpha)*np.eye(2))

# Draw w contour (from https://scipython.com/blog/visualizing-the-bivariate-gaussian-distribution/)
def multivariate_gaussian(pos, mu, sigma):
    """Return the multivariate Gaussian distribution on array pos.
'
    pos is an array constructed by packing the meshed arrays of variables
    x_1, x_2, x_3, ..., x_k into its _last_ dimension.

    """
    n = mu.shape[0]
    sigma_det = np.linalg.det(sigma)
    sigma_inv = np.linalg.inv(sigma)
    normalization = np.sqrt((2*np.pi)**n * sigma_det)
    # This einsum call calculates (x-mu)T.sigma-1.(x-mu) in a vectorized
    # way across all the input variables.
    fac = np.einsum('...k,kl,...l->...', pos-mu.squeeze(), sigma_inv, pos-mu.squeeze())

    return np.exp(-fac / 2) / normalization

N = 75
X = np.linspace(-3, 3, N)
Y = np.linspace(-1, 5, N)
X, Y = np.meshgrid(X, Y)

# Pack X and Y into a single 3-dimensional array
pos = np.empty(X.shape + (2,))
pos[:, :, 0] = X
pos[:, :, 1] = Y

Z = multivariate_gaussian(pos, mu_n, sigma_n)

# Write the file
pcf = plt.contour(X, Y, Z, levels=10)

with open("./../figures/w_density_{}.csv".format(alpha), "w") as the_file:
    the_file.write("x, y, z\n")
    for level, col in zip(pcf.levels,
                          pcf.collections):
        if level > 0:
            for path in col.get_paths():
                for polypoints in path.to_polygons():
                    for point in polypoints:
                        the_file.write("{0}, {1}, {2}\n".format(point[0], point[1], level))
        the_file.write("\n")

Phi_t = np.ones((50, 2))
Phi_t[:, 1] = np.linspace(-2,5)

mu = np.dot(Phi_t, mu_n)
sigma = np.einsum("ij,jk,ik->i", Phi_t, sigma_n, Phi_t)

dict_ = {"x": Phi_t[:, 1],
         "y": mu.squeeze(),
         "sp": mu.squeeze()+sigma,
         "sm": mu.squeeze()-sigma}

df = pd.DataFrame.from_dict(dict_)
df.to_csv("./../figures/linear_regression_bayesian.csv")

def evidence(alpha, Phi, y, sigma=1):
    A = alpha*np.eye(Phi.shape[1]) + sigma*np.dot(Phi.T, Phi)
    B = solve(A, np.dot(Phi.T, y))
    bn = np.dot(B.T, B)
    evidence = np.log(alpha) - alpha/2*bn - 0.5*np.linalg.slogdet(A)[1]
    return evidence.squeeze()

dict = {"evidence": np.array([evidence(alpha_, Phi, y)  for alpha_ in np.logspace(-3,2)]),
        "alpha": np.logspace(-3,2)}

df = pd.DataFrame.from_dict(dict)

df.to_csv("./../figures/evidence.csv")
