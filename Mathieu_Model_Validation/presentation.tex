% Created 2020-10-05 Mon 23:25
% Intended LaTeX compiler: pdflatex
\documentclass[pressentation,10pt,aspectratio=1610,xcolor=table, serif]{beamer}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\newcommand{\shorttitle}{Model assessment}
\newcommand{\shortauthor}{M. Fauvel - @MaFauvel}
\institute{CESBIO, Universit{\'e} de Toulouse, CNES/CNRS/INRAe/IRD/UPS, Toulouse, FRANCE}
\titlegraphic{\includegraphics[width=0.2\textwidth]{./figures/logo_cesbio_transp.pdf}~\hfill~\includegraphics[width=0.25\textwidth]{./figures/logo_inrae.pdf}~\hfill~\includegraphics[width=0.25\textwidth]{./figures/logo_ANITIvec.pdf}~\vspace{3cm}}
\usepackage[french, english]{babel}\usepackage{etex}\usepackage{pifont}\usepackage{booktabs}\usepackage{collcell}
\usepackage{tikz}\usepackage{amsmath, amssymb, wasysym} \usepackage[T1]{fontenc}\usepackage{lmodern}%\usepackage[babel=true,kerning=true]{microtype}
\usepackage{pgfplots,pgfplotstable}\usetikzlibrary{babel,dateplot,mindmap,trees,shapes,arrows.meta,spy,3d,backgrounds,positioning,pgfplots.statistics,calc,fit,overlay-beamer-styles, chains, quotes,shapes.geometric, decorations.markings, shapes.callouts,decorations.pathmorphing}\usepgfplotslibrary{groupplots}\pgfplotsset{compat=newest}\usepackage{minted}
\pgfplotsset{/pgf/number format/assume math mode=true}\usepackage{csquotes}
\renewenvironment{description}{\begin{itemize}}{\end{itemize}}\usepackage{smartdiagram}
\usepackage[citestyle=verbose, bibstyle=verbose, backend=biber]{biblatex}\addbibresource{refs.bib}
\usepackage[type={CC}, modifier={by-sa}, version={4.0},]{doclicense}
\usepackage{pgfpages}
\setbeameroption{hide notes}
\usetheme{Cesbio}
\author{Mathieu Fauvel}
\date{\textit{[2020-10-06 Tue]}}
\title{Model assessment and validation in data science}
\subtitle{Why my 85\% might be better than your 99\%}
\usefonttheme[onlymath]{serif}
\begin{document}

\maketitle
\begin{frame}{Outline}
\tableofcontents
\end{frame}


\begin{frame}[label={sec:org02df8f3}]{Which one is the best ?}
\begin{center}\small
  \begin{tabular}{c@{}c@{}c}
    Gaussian Process & Random Forest & Neural Nets \\
    \includegraphics[width=0.33\linewidth, height=0.22\textwidth]{figures/Gaussian_Process_blobs.pdf} &   \includegraphics[width=0.33\linewidth, height=0.22\textwidth]{figures/Random_Forest_blobs.pdf} &   \includegraphics[width=0.33\linewidth, height=0.22\textwidth]{figures/Neural_Net_blobs.pdf} \\
    0.875 &         0.925 &       0.995\\
    \visible<2->{\includegraphics[width=0.33\linewidth, height=0.22\textwidth]{figures/Gaussian_Process_blobs_val.pdf}} &   \visible<2->{\includegraphics[width=0.33\linewidth, height=0.22\textwidth]{figures/Random_Forest_blobs_val.pdf}} &   \visible<2->{\includegraphics[width=0.33\linewidth, height=0.22\textwidth]{figures/Neural_Net_blobs_val.pdf}} \\
    \visible<2->{0.825} &         \visible<2->{0.820} &       \visible<2->{0.740}
  \end{tabular}
  
  
  \begin{tikzpicture}[overlay, remember picture]
    \pgftransformshift{\pgfpointanchor{current page}{center}}
    \node<3>[
      ellipse callout,
      draw=red,
      ultra thick,
      fill=red!40,
      decoration=zigzag,
      decorate,
      callout relative pointer=(315:2cm),
      font=\Huge,
      text width=0.6\textwidth,
      align=center,
      anchor=center
    ] at (0,0) {Overfitting of the death !};
  \end{tikzpicture}
\end{center}
\end{frame}
\section{Model Assessment}
\label{sec:orgec0553a}
\begin{frame}[label={sec:orgf60d8db},standout,noframenumbering]{}
\small
This section is mainly based on \uline{Chapter 7} of
\begin{center}
\cite{hastie01statisticallearning}

\url{https://web.stanford.edu/\~hastie/ElemStatLearn/}
\end{center}
\end{frame}

\begin{frame}[label={sec:org61a5e6b}]{Machine Learning Pipeline: Split your data set !}
\begin{center}
  \begin{tikzpicture}[node distance = 0.75cm, thick, 
      base/.style = {draw, minimum width = 20mm, minimum height=15mm,  align=center, rounded corners,},
      data/.style = {base, rectangle, fill=blue!30},
      process/.style = {base, rectangle, fill=red!60},
      modelSelection/.style = {base, rectangle, fill=magenta!60},
      fat line/.style = {->, black, draw}
    ]
    \node [data]  (data)     {Data\\ \(\mathbf{X}, \mathbf{y}\)}; 
    \node [modelSelection, below= of data,fill=yellow!60] (split) {Random Split};
    \node [data, below= of split] (trainSet) {Training\\ \(\mathbf{X}_t, \mathbf{y}_t\)};
    \node [data, right= of split] (valSet) {Validation\\ \(\mathbf{X}_v, \mathbf{y}_v\)};
   \node [modelSelection, right=of trainSet] (learn) {Model Selection \\ and Fitting};
    \node [process, right= of learn] (val)  {Model Assessment};  
    \node [right=of val] (error) {Expected Error};

    \node[fit = (split)(val)(valSet), draw, rounded corners,
      inner sep=10pt, label={[shift={(3.0,0)}]Repeat \(T\) times}] (MCMC) {};

    \path[fat line] (data) -- (split);
    \path[fat line] (split) -- (trainSet);
    \path[fat line] (split) -- (valSet);
    \path[fat line] (trainSet) -- (learn);
    \path[fat line] (learn) -- (val);
    \path[fat line] (val) -- (error);
    \path[fat line] (valSet) -| (val);
  \end{tikzpicture}
\end{center}
\end{frame}

\begin{frame}[label={sec:org242524a}]{Model Error}
\begin{block}{Cost function: \(L\big(f(\mathbf{x}_i), y_i\big)\)}
\begin{itemize}
\item Regression:
\begin{itemize}
\item Squared loss: \(\big(f(\mathbf{x}_i)- y_i\big)^2\)
\item Absolute loss: \(\big|f(\mathbf{x}_i)- y_i|\)
\end{itemize}
\item Classification
\begin{itemize}
\item Indicator: \(I(f(\mathbf{x}_i), y_i)= 1 \text{ if } f(\mathbf{x}_i)=y_i \text{ else } 0\)
\item Cross-entropy: \(\sum_{c=1}^C I(y_i, c)\log\Big(p_{y_i=c}\big(f(\mathbf{x}_i)\big)\Big)\)
\end{itemize}
\end{itemize}
\end{block}

\begin{block}<2->{Error estimation}
\begin{itemize}
\item Training error: \(E_{\mathcal{S}} = \frac{1}{|\mathcal{S}|}\sum_{i\in\mathcal{S}}L\big(\hat{f}(\mathbf{x}_i), y_i\big)\)

\item Test error (generalization error): \(E_{\mathcal{S}} = \mathbb{E}\bigg[L\big(\hat{f}(\mathbf{X}), Y\big)|\mathcal{S}\bigg]\)

\item Expected prediction error: \(E = \mathbb{E}(E_{\mathcal{S}})\)
\end{itemize}
\end{block}
\end{frame}

\begin{frame}[label={sec:org44f1827}]{Bias-variance trade-off and model complexity}
\begin{block}{Bias-Variance decomposition}
\begin{itemize}
\item \(Y = f(\mathbf{X}) + \varepsilon\) with \(\varepsilon \sim \mathcal{N}(0, \sigma^2)\)
\item Expected prediction error at \(\mathbf{x}\):
  \begin{eqnarray*}
  E(\mathbf{x}) & = & \mathbb{E}\big[\big(Y - \hat{f}(\mathbf{X})\big)^2 | \mathbf{X} = \mathbf{x}\big] \\
  &=& \Big(f(\mathbf{x}) - \mathbb{E}[\hat{f}(\mathbf{x})]\Big)^2 + \mathbb{E}\Big[\hat{f}(\mathbf{x}) - \mathbb{E}[\hat{f}(\mathbf{x})]\Big]^2 + \mathbb{E}(\varepsilon^2)\\
&\visible<2->{=}&\visible<2->{\text{Bias}^2 + \text{ Variance } + \sigma^2}
  \end{eqnarray*}
\end{itemize}
\end{block}

\begin{block}<3->{Model complexity}
\begin{itemize}
\item Defines as the number of \textbf{free} parameters of the model
\begin{center}
\footnotesize
\begin{tabular}{ll}
\toprule
Model Complexity & \(\nearrow\)\\
\midrule
Bias & \(\searrow\)\\
Variance & \(\nearrow\)\\
\bottomrule
\end{tabular}
\end{center}
\item \(K\)-Nearest-Neighbors regressor \vspace{-0.5cm} \[E(\mathbf{x}) = \sigma^2 + \Big[f(\mathbf{x}) - \frac{1}{K}\sum_{l=1}^Kf(\mathbf{x}_l)\Big]^2 + \frac{\sigma^2}{K}\]
\end{itemize}
\end{block}

\begin{center}
  \begin{tikzpicture}[overlay, remember picture]
    \pgftransformshift{\pgfpointanchor{current page}{center}}
    \node<4>[
      ellipse callout,
      draw=red,
      ultra thick,
      fill=red!40,
      decoration=zigzag,
      decorate,
      callout relative pointer=(315:2cm),
      font=\Huge,
      text width=0.6\textwidth,
      align=center,
      anchor=center
    ] at (0,0) {A too complex model learns the training data !};
  \end{tikzpicture}
\end{center}
\end{frame}

\begin{frame}[label={sec:org39c43d6}]{Example - Learn Sinc + Noise with KNN}
\begin{center}
  \begin{tikzpicture}
    \begin{axis}[grid,small, width=0.5\linewidth, height=0.25\linewidth, xmin=0, xmax=40, ymin= 0,ymax=0.05,x dir=reverse,
        ytick=={0, 0.025,0.05},
        yticklabel style={
          /pgf/number format/.cd,
          fixed,
          fixed zerofill,
          precision=1,
          /tikz/.cd
        },
        xlabel={Number of neighbors}, ylabel={Mean square error}, mlineplot, disable thousands separator]
      \addplot[thick, blue] table[x=k, y=err_train, col sep=comma] {figures/knn_train_test_err.csv};
      \addplot[thick, red] table[x=k, y=err_test, col sep=comma] {figures/knn_train_test_err.csv};
    \end{axis}
  \end{tikzpicture}
\end{center}

\begin{center}
  \begin{tabular}{ccc}
    \begin{tikzpicture}
      \begin{groupplot}[grid, small,group style={group size=3 by 1, yticklabels at=edge left,}, width=0.35\linewidth, height=0.25\linewidth, mlineplot, disable thousands separator]
        \nextgroupplot[title={\(K=1\)}]
        \addplot[thick, black] table [col sep = comma, x=t, y=pred] {figures/knn_pred_1.csv};
        \addplot[thick, dashed, gray] table [col sep = comma, x=t, y=true] {figures/knn_pred_1.csv};
        \nextgroupplot[title={\(K=8\)}]
        \addplot[thick, black] table [col sep = comma, x=t, y=pred] {figures/knn_pred_8.csv};
        \addplot[thick, dashed, gray] table [col sep = comma, x=t, y=true] {figures/knn_pred_8.csv};
        \nextgroupplot[title={\(K=39\)}]
        \addplot[thick, black] table [col sep = comma, x=t, y=pred] {figures/knn_pred_39.csv};
        \addplot[thick, dashed, gray] table [col sep = comma, x=t, y=true] {figures/knn_pred_39.csv};
      \end{groupplot}
    \end{tikzpicture}
  \end{tabular}
\end{center}
\end{frame}

\section{Tools}
\label{sec:org69816d0}
\begin{frame}[label={sec:orgc4f477e},standout,noframenumbering]{}
\small
This section is mainly based on tools from:
\begin{center}
\cite{scikit-learn}

\url{https://scikit-learn.org/stable/index.html}
\end{center}
\end{frame}
\begin{frame}[label={sec:org72fa7d9}]{Cross validation: Smartly split your data}
\begin{block}{Principle}
\begin{itemize}
\item Based on splitting the data in \(K\)-(disjoint)folds :
\begin{center}
      \begin{tabular}{l*5{p{5em}}}
        Model\(_1\) & \cellcolor{orange} Train & \cellcolor{orange} Train & \cellcolor{orange} Train & \cellcolor{orange} Train & \cellcolor{blue!40} Validation \\
        Model\(_2\) & \cellcolor{orange} Train & \cellcolor{orange} Train & \cellcolor{orange} Train & \cellcolor{blue!40} Validation & \cellcolor{orange} Train \\
        Model\(_3\) & \cellcolor{orange} Train & \cellcolor{orange} Train & \cellcolor{blue!40} Validation & \cellcolor{orange} Train & \cellcolor{orange} Train \\
        Model\(_4\) & \cellcolor{orange} Train & \cellcolor{blue!40} Validation & \cellcolor{orange} Train & \cellcolor{orange} Train & \cellcolor{orange} Train \\
        Model\(_5\) & \cellcolor{blue!40} Validation & \cellcolor{orange} Train & \cellcolor{orange} Train & \cellcolor{orange} Train & \cellcolor{orange} Train 
      \end{tabular}
\end{center}
\item Estimation of the \emph{expected prediction error}
\[CV(\hat{f},\theta) = \frac{1}{K}\sum_{k=1}^K\text{Err}_k(\hat{f},\theta)\]
\end{itemize}
\end{block}
\end{frame}

\begin{frame}[label={sec:orgaed9b55}]{Good practices}
\begin{itemize}
\item \(K ?\) Usually K=5 or 10 is a good trade-off (K=n is called leave-one-out)
\begin{center}
\footnotesize
\begin{tabular}{lll}
\toprule
 & Bias & Variance\\
\midrule
K low & High & Low\\
K high & Low & High\\
K = n & Low & Very High\\
\bottomrule
\end{tabular}
\end{center}
\item Be careful to the learning curve
\end{itemize}
\begin{center}
      \begin{tikzpicture}
        \begin{axis}[domain=0:500,samples=300,footnotesize, grid, xmin=0, xmax=500,width=0.6\linewidth,height=0.28\linewidth,ymin=0,ymax=1.1,
        xlabel=Number of training samples, ylabel=Accuracy, mlineplot, disable thousands separator]
          \addplot[mark=none, blue, very thick] {1-exp(-x/200)};
          \addplot[mark=none, magenta, very thick] {1/(1+exp(-(x-400)/10))};
          \addplot[mark=none, orange, very thick] {0.5-0.5*exp(-x/10)};
        \end{axis}
      \end{tikzpicture}
      \end{center}
\begin{itemize}
\item Model should \textbf{be trained completely} for each fold (e.g., data normalization, optimization)
\end{itemize}
\end{frame}

\begin{frame}[label={sec:org9453ab8},fragile]{Notebook: Cross Validation for model fitting}
 \begin{onlyenv}<1>
\begin{minted}[fontsize=\footnotesize,obeytabs=true,tabsize=4,bgcolor=bg,breaklines=true]{python}
X,y = make_classification(n_samples=1000, n_features=50,
                          n_informative=40, n_redundant=10,
                          n_classes=5, n_clusters_per_class=2)
# Standardize data
sc = MinMaxScaler()
X = sc.fit_transform(X) # Scale data between 0 and 1
X_train, X_test, y_train, y_test = train_test_split(
    X, y, train_size=0.75, random_state=0)

# Hyperparameters
C = 10.0**sp.arange(-1, 3) # Penality of the optimization problem
gamma = 2.0**sp.arange(-4, 4) # Scale of the RBF kernel
params = dict(kernel=['rbf'], gamma=gamma, C=C)

# Do Cross Validation
grid = GridSearchCV(SVC(),  # Set up the classifier
                    param_grid=params, cv= 5,
                    refit=True, n_jobs=-1) # Do the grid search in parallel
grid.fit(X_train, y_train) # Run the grid search
\end{minted}
\end{onlyenv}

\begin{onlyenv}<2>
\begin{center}
  \begin{tikzpicture}
    \begin{axis}[view={0}{90}, colorbar, mesh/rows=4, mesh/cols=8,xmin=-0.5, xmax=7.5,ymin=-0.5,ymax=3.5,
    title= {Expected test error=0.77 \(\pm 0.03\) | Test error=0.80},
      xtick={0,1,2,3,4,5,6,7},
      xticklabels={\(2^{-4}\), \(2^{-3}\) , \(2^{-2}\), \(2^{-1}\)   , \(2^{0}\), \(2^{1}\)    , \(2^{2}\)    , \(2^{3}\)},
      ytick={0,1,2,3},
      yticklabels={\(10^{-1}\), \(10^{0}\),  \(10^{1}\), \(10^{2}\)},
      ]
      \addplot [matrix plot, point meta=explicit] table [meta=z] {figures/cv_err_1.csv};
      \addplot[mark=*, black] coordinates {(4, 2)};
    \end{axis}
  \end{tikzpicture}
\end{center}
\end{onlyenv}
\end{frame}
\begin{frame}[label={sec:orgfba24e1}]{Bootstrap}
\begin{block}{Principle}
\begin{itemize}
\item Bootstrap dataset \(\mathcal{S}^b\): draw with replacement from the training data \(\mathcal{S}\)
\[\mathcal{S}^b \in \mathcal{S}\]
\item Monte-Carlo error estimation
\[\text{BS}(f)=\frac{1}{BN}\sum_{b,i=1}^{B,N}L(y_i, \hat{f}^b(\mathbf{x}_i))\]
\item Grouped MC error estimation
\[\text{BS}_g(f)=\frac{1}{BN_b}\sum_{b,i\notin b}^{B}L(y_i, \hat{f}^b(\mathbf{x}_i))\]
\end{itemize}
\end{block}
\end{frame}
\begin{frame}[label={sec:org4059e64},fragile]{Notebook: Bootstrap  for models comparison}
 \begin{onlyenv}<1>
\begin{minted}[fontsize=\footnotesize,obeytabs=true,tabsize=4,bgcolor=bg,breaklines=true]{python}
classifiers = [SVC(kernel="rbf", gamma=1, C=10),
               RF(n_estimators=500, n_jobs=1),
               NNC(hidden_layer_sizes=(100, 50, 40,),
                   random_state=0, alpha=0.1, max_iter=500)]

def compute_bootstrap(random_state, X, y, classifiers):
    X_train, X_test, y_train, y_test = train_test_split(
        X, y, train_size=0.75, test_size=0.25,
        random_state=random_state)
    local_res = []
    for clf in classifiers:
        clf.fit(X_train, y_train)
        local_res.append(clf.score(X_test, y_test))
    return local_res

res =  Parallel(n_jobs=-1)(delayed(compute_bootstrap)(
    rs, X, y, classifiers) for rs in range(50))
\end{minted}
\end{onlyenv}
\begin{onlyenv}<2>
\begin{center}
  \begin{tikzpicture}
    \begin{axis}[grid,small, width=0.85\linewidth, height=0.25\linewidth, xmin=0, xmax=49, ymin=0.5, ymax=0.8,
    xlabel=Iterations, ylabel=Accuracy,
    legend style={fill=none, at={(0.5,1.03)},anchor=south}, legend columns=3]
      \addplot[very thick, mark=none, orange] table[x=iterations, y=SVC, col sep=comma] {figures/res_bootstrap.csv};
      \addplot[very thick, mark=none, blue] table[x=iterations, y=RF, col sep=comma] {figures/res_bootstrap.csv};
      \addplot[very thick, mark=none, black] table[x=iterations, y=NN, col sep=comma] {figures/res_bootstrap.csv};
      \legend{SVM, RF, NN}
    \end{axis}
  \end{tikzpicture}
\end{center}

\begin{itemize}
\item Wilcoxon signed-rank test: (H0) the median difference between pairs of observations is zero.
\begin{center}
\begin{tabular}{ll}
\toprule
 & \(p\)-value\\
\midrule
SVC-RF & \(\approx\) 0\\
SVC-NN & \(\approx\) 0\\
RF-NN & 0.629\\
\bottomrule
\end{tabular}
\end{center}
\end{itemize}
\end{onlyenv}
\end{frame}

\section{Special case of Spatial Data}
\label{sec:org062b327}
\begin{frame}[label={sec:orgd35fcd6},standout,noframenumbering]{}
\small
This section is mainly based on
\begin{itemize}
\item \uline{Chapter 2} of:
\begin{center}
\cite{cressie-1993-statis-spatial-data}
\end{center}
\item \uline{Unreferenced} material: 
\begin{center}
\cite{spatial:cv}
\end{center}
\end{itemize}
\end{frame}

\begin{frame}[label={sec:org561916d}]{Spatial auto-correlation}
\begin{block}{What is spatial auto-correlation ?}
\begin{center}
  \begin{tabular}{ccc}
    \includegraphics[width=0.15\linewidth]{figures/spatialauto_0.01.pdf} &
    \includegraphics[width=0.15\linewidth]{figures/spatialauto_1.pdf} &
    \includegraphics[width=0.15\linewidth]{figures/spatialauto_100.pdf}
  \end{tabular}
\end{center}
\begin{center}
``Spatial autocorrelation is defined as the correlation of a variable with itself due to the spatial location of the observations''.
\end{center}
\end{block}
\begin{block}{Challenges in error estimation}
\begin{enumerate}
\item How to ensure idependence between training and validation samples  ?
\item Take/Not take into account the spatial auto-correlation structure ?
\item Interpolation or extrapolation in the spatial domain ?
\end{enumerate}
\end{block}
\end{frame}
\begin{frame}[label={sec:org7aa3139}]{Spatial cross-validation}
\begin{itemize}
\item Folds are defined using spatial boundaries
\end{itemize}

\begin{center}
  \begin{tabular}{cc}
    \begin{tikzpicture}
      \fill[gray] (0,4) rectangle (2,6);    
      \fill[gray!50] (0,2) rectangle (2,4);    
      \fill[gray!50] (2,2) rectangle (4,4);    
      \fill[gray!50] (2,4) rectangle (4,6);
      
      \draw[thick] (0,0) rectangle (6,6);
      \foreach \x in {2,4}{
        \draw[dashed] (\x, 0) -- (\x, 6);
        \draw[dashed] (0, \x) -- (6, \x);
      }
      
      \pgfmathsetseed{1}
      \foreach \iter in {1,...,40}{
        \pgfmathsetmacro\a{rnd*6}
        \pgfmathsetmacro\b{rnd*6}
        %% \pgfmathparse{%
        %%   ifthenelse(\a > 0 && \a < 2 && \b > 4 && \b < 6,%
        %%   "blue", % 
        %%   ifthenelse(\a > 0 && \a < 4 && \b > 2 && \b < 4,%
        %%   "red", % 
        %%   ifthenelse(\a > 2 && \a < 4 && \b > 4 && \b < 6,%
        %%   "red", %
        %%   "black")))}
        \pgfmathparse{%
          ifthenelse(\a < 2,
          ifthenelse(\b > 4,
          "blue",
          "black"),
          "black")}
        \fill[\pgfmathresult] (\a,\b) circle (0.1);
      }
      \fill[black] (2, 2) circle (0.1);
      \fill[black] (4, 4) circle (0.1);      
  \end{tikzpicture} & 
    \begin{tikzpicture}
      \draw[thick] (0,0) rectangle (6,6);
      
      \newcommand{\cX}{2}
      \newcommand{\cY}{2}
      \newcommand{\cR}{1.5}
      
      \fill[gray!50] (\cX,\cY) circle (\cR);
      \fill[blue] (\cX,\cY) circle (0.1);

      \newcommand{\cXb}{4}
      \newcommand{\cYb}{4}
      
      \fill[gray!50] (\cXb,\cYb) circle (\cR);
      \fill[blue] (\cXb,\cYb) circle (0.1);
      
      % Check if numbers are inside circle
      \pgfmathsetseed{1}
      \foreach \x in {1,...,40}{
        \pgfmathsetmacro\a{rnd*6}
        \pgfmathsetmacro\b{rnd*6}
        \pgfmathparse{ifthenelse((\a-\cX)^2 + (\b-\cY)^2 <= \cR^2,%
          "red",
          ifthenelse((\a-\cXb)^2 + (\b-\cYb)^2 <= \cR^2,
          "red",
          "black"))}
        \fill[\pgfmathresult] (\a,\b) circle (0.1);
      }
    \end{tikzpicture}
    \\
    Grid & Buffered K-CV
  \end{tabular}
\end{center}
\end{frame}
\begin{frame}[label={sec:org442db82},fragile]{OSO uses polygons}
 \begin{columns}
\begin{column}{0.6\columnwidth}
\begin{center}
\includegraphics[width=0.6\linewidth]{./figures/ref_data.pdf}
\end{center}
\end{column}

\begin{column}{0.55\columnwidth}
\begin{minted}[fontsize=\footnotesize,obeytabs=true,tabsize=4,bgcolor=bg,breaklines=true]{python}
X = df[bands].values
y = df["class"].values
groups = df["originfid"].values

kfold = KFold(n_splits=2, shuffle=True)
group_kfold = GroupKFold(n_splits=2)
\end{minted}
\end{column}
\end{columns}
\begin{visibleenv}<2->
\begin{itemize}
\item Accuracy without spatial: 0.9915536819718203
\item Accuracy with spatial: 0.8176180167862975
\end{itemize}
\end{visibleenv}
\end{frame}
\begin{frame}[label={sec:org2ec758d},standout,noframenumbering]{}
\small
On going work \ldots{}

\definecolor{cec1d24}{RGB}{236,29,36}
\definecolor{cffffff}{RGB}{255,255,255}
\begin{center}
  \begin{tikzpicture}[background rectangle/.style={CESBIOgreen}, show background rectangle,%
      y=3pt,x=3pt,yscale=-0.05,xscale=0.05, inner sep=0pt, outer sep=0pt]
       {\path[fill=cec1d24,nonzero rule] (635.8833,600.0000) .. controls
         (651.0771,599.6647) and (665.7558,591.6224) .. (673.9783,577.5525) .. controls
         (682.1001,563.3625) and (681.7384,546.6500) .. (674.5074,533.1862) --
         (378.4699,21.6487) .. controls (370.6590,8.7662) and (356.3424,0.0975) ..
         (340.1099,-0.0063) .. controls (323.6499,0.0972) and (309.3362,8.7657) ..
         (301.4862,21.6487) -- (5.4612,533.1862) .. controls (-1.7257,546.6500) and
         (-2.0877,563.3625) .. (5.9903,577.5525) .. controls (14.2570,591.6225) and
         (28.9353,599.6650) .. (44.0853,600.0000) -- (635.8853,600.0000);
         \path[fill=cffffff,nonzero rule] (340.1208,75.7875) -- (71.0683,540.8450) --
         (608.8933,540.8450) -- (340.1058,75.7950);
         \path[fill=black,nonzero rule] (303.5900,225.7800) .. controls
         (280.4500,225.7300) and (276.9200,248.1400) .. (276.8800,250.6200) .. controls
         (276.9200,262.4400) and (285.4000,277.1700) .. (309.1600,279.4100) .. controls
         (309.1600,279.4100) and (313.7700,280.0900) .. (313.6600,283.0900) .. controls
         (313.7700,285.9400) and (313.7800,284.9700) .. (312.3400,286.5300) .. controls
         (310.8500,288.2300) and (298.5500,299.6400) .. (298.3100,302.6600) .. controls
         (296.9800,316.8800) and (300.1800,335.2800) .. (303.0900,345.9400) .. controls
         (303.0900,345.9400) and (306.6000,354.7800) .. (303.8800,361.0000) .. controls
         (301.0600,367.4700) and (289.0000,397.7400) .. (288.0000,399.5600) .. controls
         (285.1300,404.8200) and (284.0000,409.1400) .. (285.8800,415.4100) .. controls
         (284.6600,416.1100) and (264.4700,428.8800) .. (264.4700,428.8800) .. controls
         (264.4700,428.8800) and (261.3500,421.5100) .. (253.5900,419.3800) .. controls
         (245.7000,417.1100) and (239.1800,418.1800) .. (232.7200,405.9100) --
         (226.8800,396.4100) .. controls (226.8800,396.4100) and (225.6700,392.9300) ..
         (219.7500,391.6200) .. controls (213.9300,390.4900) and (206.1000,388.3000) ..
         (202.8100,388.4700) .. controls (198.6100,388.8800) and (195.2200,387.7300) ..
         (189.5900,394.2800) .. controls (185.0800,399.5300) and (112.8800,524.4700) ..
         (112.8800,524.4700) -- (286.4100,524.4700) .. controls (286.4100,524.4700) and
         (290.7100,524.0000) .. (289.5900,519.9700) .. controls (288.2700,516.1900) and
         (267.3800,441.8100) .. (267.3800,441.8100) -- (291.4400,426.7500) .. controls
         (291.4400,426.7500) and (293.3600,429.9900) .. (300.4400,426.7500) .. controls
         (307.3800,423.4800) and (309.9700,421.7500) .. (309.9700,421.7500) .. controls
         (309.9700,421.7500) and (313.9200,421.4900) .. (313.4100,413.0300) .. controls
         (316.1700,411.3100) and (341.9700,395.0600) .. (341.9700,395.0600) .. controls
         (341.9700,395.0600) and (332.8400,417.6000) .. (331.9100,427.5600) .. controls
         (331.2100,437.4600) and (327.6900,504.1200) .. (327.6900,504.1200) .. controls
         (327.6900,504.1200) and (328.1300,509.2200) .. (324.7800,510.2200) .. controls
         (321.2800,511.1700) and (305.7200,515.5000) .. (305.7200,515.5000) .. controls
         (305.7200,515.5000) and (301.3900,516.2100) .. (301.5000,519.7200) .. controls
         (301.3900,523.0400) and (303.0200,524.3400) .. (304.9400,524.4700) .. controls
         (306.9300,524.3400) and (355.7200,524.4700) .. (355.7200,524.4700) .. controls
         (355.7200,524.4700) and (360.7100,525.0000) .. (361.5300,518.4100) .. controls
         (362.3400,511.9900) and (371.5900,445.5000) .. (371.5900,445.5000) .. controls
         (371.5900,445.5000) and (367.5700,444.4500) .. (367.6200,440.5000) .. controls
         (367.5700,436.3200) and (367.5600,433.2000) .. (368.9400,431.5000) .. controls
         (370.1600,429.9500) and (384.8300,413.0400) .. (394.8800,389.5300) .. controls
         (396.4200,386.0300) and (397.5600,389.1300) .. (397.7800,390.0600) .. controls
         (398.2100,390.7500) and (417.2800,442.6600) .. (419.4700,444.7200) .. controls
         (421.8400,446.5600) and (473.3600,488.2300) .. (474.5000,489.0900) .. controls
         (475.6400,489.8600) and (478.9100,492.1100) .. (479.0000,499.3800) .. controls
         (478.9100,506.7600) and (475.9700,512.6300) .. (473.9700,514.9700) .. controls
         (472.0500,517.1900) and (468.8000,520.4500) .. (468.6900,521.8400) .. controls
         (468.8000,523.3800) and (470.2800,524.3400) .. (471.3400,524.4700) .. controls
         (472.5600,524.3400) and (479.2500,524.3400) .. (479.8100,524.4700) .. controls
         (480.5500,524.3400) and (489.3400,525.0100) .. (495.4100,514.1900) .. controls
         (501.7300,503.5300) and (513.1200,484.3400) .. (513.1200,484.3400) .. controls
         (513.1200,484.3400) and (515.5800,480.5600) .. (512.0600,478.2500) .. controls
         (508.4000,476.0100) and (502.7100,474.7100) .. (499.3800,471.1200) .. controls
         (495.8600,467.5500) and (462.9400,429.6400) .. (462.3400,428.8800) .. controls
         (461.9600,428.3400) and (457.7100,423.7600) .. (452.2800,426.2200) .. controls
         (450.7000,424.0900) and (448.8400,421.2200) .. (448.8400,421.2200) .. controls
         (448.8400,421.2200) and (439.4600,364.4600) .. (429.5300,340.4100) .. controls
         (431.8000,339.0000) and (434.8100,336.7200) .. (434.8100,336.7200) .. controls
         (434.8100,336.7200) and (442.7200,343.5500) .. (449.3800,336.4400) .. controls
         (456.0900,329.2300) and (455.6000,329.4100) .. (454.4100,326.1600) .. controls
         (453.3200,322.9000) and (452.8300,321.9100) .. (450.4400,320.5900) .. controls
         (448.2700,319.3000) and (444.5200,315.5500) .. (444.6200,308.9700) .. controls
         (444.5200,302.5400) and (441.7400,284.8000) .. (438.5300,277.2800) .. controls
         (435.5500,269.8300) and (434.5600,261.5500) .. (422.9100,256.4400) .. controls
         (404.4800,248.2100) and (379.8000,243.3100) .. (361.8100,247.9700) .. controls
         (356.8300,249.3000) and (340.8200,261.0500) .. (339.3100,261.9700) .. controls
         (337.8900,262.6800) and (331.6400,265.2600) .. (332.1900,259.8400) .. controls
         (333.4500,247.4700) and (322.7500,225.7300) .. (303.5900,225.7800) --
         cycle(394.8800,275.9700) .. controls (394.8800,275.9700) and
         (408.4700,275.8400) .. (411.5300,276.2200) .. controls (414.3400,276.5000) and
         (416.0300,280.1900) .. (416.0300,280.1900) -- (429.0000,325.8800) --
         (424.2500,328.7800) .. controls (421.5800,323.2700) and (397.9000,287.2600) ..
         (393.0300,280.4700) .. controls (389.9100,276.1000) and (394.8800,275.9700) ..
         (394.8800,275.9700) -- cycle(331.0000,332.8100) .. controls
         (331.5700,332.7900) and (332.2200,332.9700) .. (332.7200,333.2800) .. controls
         (337.8300,336.2000) and (347.5100,344.5400) .. (355.7200,356.0000) .. controls
         (356.7000,357.3200) and (355.7200,361.2800) .. (355.7200,361.2800) --
         (348.5900,376.0600) .. controls (348.5900,376.0600) and (317.8500,395.3700) ..
         (315.5000,396.9100) .. controls (312.9600,398.3000) and (311.5500,396.9300) ..
         (313.9400,393.7500) .. controls (327.6400,375.1200) and (332.7200,353.0000) ..
         (329.5300,335.1200) .. controls (329.2600,333.4800) and (330.0500,332.8500) ..
         (331.0000,332.8100) -- cycle;}
  \end{tikzpicture}
\end{center}
\end{frame}

\begin{frame}[label={sec:orgca61eaf}]{Semi-variogram: A tool to assess spatial auto-correlation}
\begin{itemize}
\item Describes the degree of spatial dependence between two sample as a function of the spatial distance \(\ell\).
\end{itemize}

\begin{eqnarray*}
  \gamma(\ell\pm\delta) = \frac{1}{2|N(\ell\pm\delta)|} \sum_{(i,j)\in N(\ell\pm\delta)} |x_i - x_j|^2
\end{eqnarray*}

\begin{figure}
  \centering
  \begin{tikzpicture}
    \begin{axis}[domain=0:100, samples=100, grid, xmin=0,xmax=100, xlabel=\(\ell\), small,ymin=0,ymax=120,width=0.75\linewidth, height=0.4\linewidth, mlineplot, disable thousands separator]
      \addplot[thick, blue] {(100-25)*(1-exp(-x/10)) + 25};      
      \addplot[thick, red, dashed, <->] coordinates {(1, 25) (1, 0)} node[pos=0.5, right] {\footnotesize Nugget};
      \addplot[thick, red, dashed] coordinates {(0, 100) (100, 100)} node[pos=0.5, above] {\footnotesize Sill};
      \addplot[thick, red, dashed, <->] coordinates {(0, 40) (60, 40)} node[pos=0.80, above] {\footnotesize Range};
    \end{axis}
  \end{tikzpicture}
  
  \caption{Simulated semivariogram.}
  \label{fig:variogram}  
\end{figure}
\end{frame}
\begin{frame}[label={sec:org9c29337}]{Test case on FORMOSAT data}
\begin{center}
  \only<1>{\includegraphics[width=0.7\linewidth]{figures/formosat2010.pdf}}
  \only<2>{
    \begin{tikzpicture}
      \begin{axis}[small, grid, xmin=0, xmax=20000, width=0.95\linewidth, height=0.5\linewidth, ymin=0, ymax=240000, xlabel=\(\ell\), ylabel={\(\gamma(\ell\pm 50)\)}, mlineplot, disable thousands separator]
        \addplot[only marks, blue] table[x=LAG, y=SV, col sep=comma] {figures/semi_variogram.csv};      
        \addplot[domain=0:20000, samples=1000, red, thick] {201629*(1-exp(-4*(x^2)/(7935^2)))};
        \addplot[black, thick] coordinates {(7935, 0) (7935, 240000)};
      \end{axis}
  \end{tikzpicture}}
  \only<3->{\includegraphics[width=0.5\linewidth]{figures/sample_selection.pdf}}
\end{center}

\begin{center}
  \begin{tikzpicture}[overlay, remember picture]
    \pgftransformshift{\pgfpointanchor{current page}{center}}
    \node<4>[
      ellipse callout,
      draw=red,
      ultra thick,
      fill=red!40,
      decoration=zigzag,
      decorate,
      callout relative pointer=(315:2cm),
      font=\Huge,
      text width=0.6\textwidth,
      align=center,
      anchor=center
    ] at (0,0) {It might be more complicated than that \frownie{}};
  \end{tikzpicture}
\end{center}
\end{frame}
\section{Conclusion}
\label{sec:orgf43ec45}
\begin{frame}[label={sec:org91d79d1},fragile]{Take home message}
 \begin{itemize}
\item Model fitting and model selection: \textbf{Cross-validation}
\item Model assessement: \textbf{Bootstrap}
\item Random split: 
\begin{enumerate}
\item Uniform
\item Grouped per polygons
\item Grouped per ad-hoc spatial boundaries
\end{enumerate}
\item Most of the tools exist (\texttt{scikit-learn})
\end{itemize}
\end{frame}
\begin{frame}[fragile,allowframebreaks,label=ref]{References}
\printbibliography[heading=none]
\end{frame}

\begin{frame}[label={sec:org22747ab},noframenumbering]{Licence}
\begin{center}
\small
\doclicenseLongText

\doclicenseImage
\end{center}
\end{frame}
\end{document}
